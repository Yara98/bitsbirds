<?php
require(APPPATH . '/libraries/REST_Controller.php');
require(APPPATH . '/libraries/Format.php');

class Texto_api extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');

        $this->load->model('Textos_model');
    }

    public function recuperaContenidoTexto_get()
    {
        $id_texto = $this->get('id_texto');
        $resultado = $this->Textos_model->recuperaContenidoTexto($id_texto);
        if ($resultado != null) {
            $this->response($resultado, 200);
        } else {
            $this->response("No se encontraron textos en común", 400);
        }
    }

    public function recuperaTextos_get()
    {
        $resultado = $this->Textos_model->recuperaTextos();
        if ($resultado != null) {
            $this->response($resultado, 200);
        } else {
            $this->response("No se encontraron textos en DB", 400);
        }
    }

    public function recuperaTexto_get()
    {
        $resultado = $this->Textos_model->recuperaTexto();
        if ($resultado != null) {
            $this->response($resultado, 200);
        } else {
            $this->response("No se encontró el texto en DB", 400);
        }
    }

    public function recuperaTexto2_get()
    {
        $resultado = $this->Textos_model->recuperaTexto2();
        if ($resultado != null) {
            $this->response($resultado, 200);
        } else {
            $this->response("No se encontró el texto en DB", 400);
        }
    }

    public function recuperaTexto3_get()
    {
        $resultado = $this->Textos_model->recuperaTexto3();
        if ($resultado != null) {
            $this->response($resultado, 200);
        } else {
            $this->response("No se encontró el texto en DB", 400);
        }
    }

    public function recuperaTexto4_get()
    {
        $resultado = $this->Textos_model->recuperaTexto4();
        if ($resultado != null) {
            $this->response($resultado, 200);
        } else {
            $this->response("No se encontró el texto en DB", 400);
        }
    }

    public function recuperaTexto5_get()
    {
        $resultado = $this->Textos_model->recuperaTexto5();
        if ($resultado != null) {
            $this->response($resultado, 200);
        } else {
            $this->response("No se encontró el texto en DB", 400);
        }
    }

    public function actualizaTexto_put()
    {
        $texto = $this->put('texto');
        $id_texto = $this->put('id_texto');
        
        $actualizaTexto = $this->Textos_model->actualizaTexto($texto, $id_texto);

        if ($actualizaTexto) {
            $this->response("Texto actualizado", 200);
        } else {
            $this->response("Texto actualizado", 400);
        }
    }
}
