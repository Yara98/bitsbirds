<?php
class Textos_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    /** FUNCIONES PARA LA RECUPERACIÓN DE TEXTOS */

    public function recuperaContenidoTexto($id_texto){
        $query = "SELECT * FROM textos where id_texto = '" . $id_texto . "' ";
        $resultado = $this->db->query($query);
        $resultado = $resultado->row_array();

        return $resultado;
    }

    public function recuperaTextos(){
        $query = "SELECT * FROM textos";
        $resultado = $this->db->query($query);
        $resultado = $resultado->result_array();

        return $resultado;
    }

    public function recuperaTexto()
    {
        $query = "SELECT * FROM textos where id_texto = 1";
        $resultado = $this->db->query($query);
        $resultado = $resultado->row_array();

        return $resultado;
    }

    public function recuperaTexto2()
    {
        $query = "SELECT * FROM textos where id_texto = 2";
        $resultado = $this->db->query($query);
        $resultado = $resultado->row_array();

        return $resultado;
    }
    
    public function recuperaTexto3()
    {
        $query = "SELECT * FROM textos where id_texto = 3";
        $resultado = $this->db->query($query);
        $resultado = $resultado->row_array();

        return $resultado;
    }

    public function recuperaTexto4()
    {
        $query = "SELECT * FROM textos where id_texto = 4";
        $resultado = $this->db->query($query);
        $resultado = $resultado->row_array();

        return $resultado;
    }

    public function recuperaTexto5()
    {
        $query = "SELECT * FROM textos where id_texto = 5";
        $resultado = $this->db->query($query);
        $resultado = $resultado->row_array();

        return $resultado;
    }

    public function actualizaTexto($texto, $id_texto){
        $query = "UPDATE textos SET contenido = '" . $texto . "' WHERE id_texto = '" . $id_texto . "' ";
        $resultado = $this->db->query($query);

        return $resultado;
    }
    
}




