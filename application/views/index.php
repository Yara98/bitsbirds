<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bits&Birds</title>

  <base href="<?php echo $this->config->base_url() ?>" />

  <!-- LINK PARA RECONOCER ICONOS DE BOOTSTRAP -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

  <script type="text/javascript" src="assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/popper.min.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <!-- Estilos Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <!-- Estilos CSS -->
  <link rel="stylesheet" href="assets/css/home.css">

</head>

<body>
  <div id="index">

    <nav class="navbar navbar-expand-lg navbar-dark " class="navbar">
      <div class="container-fluid">
        <a class="navbar-brand text-center col-3 col-sm-3 col-md-2 col-lg-3" href="<?php echo $this->config->site_url() . '/Home/index' ?>">
          <img class="" src="assets/img/BAB.png">
        </a>
        <a class="nav-link mr-5  col-lg-4 nav-font" href="">EN</a>

        <a class="nav-link  col-lg-1 nav-font" href="#" id="encabezado2">SERVICE</a>
        <a class="nav-link  col-lg-1 nav-font" href="#" id="encabezado1">CLIENTS</a>
        <a class="nav-link  col-lg-1 nav-font" href="#" id="encabezado">ABOUT US</a>
        <button class="get  col-lg-1 nav-font">GET IN TOUCH</button>
        <a class="col-lg-1"></a>
        <!-- <div class="col-lg-2 col-md-2 col-sm-12"></div> -->
        <!-- col-6 col-sm-6 col-md-12 col-lg-6 -->
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">

        <div class="col-12 col-sm-12 col-md-12 col-lg-12" id="header" style="background-image: url('assets/img/back.png'); width: 100%; height: 85vh; background-repeat: no-repeat; background-size: cover;">
          <div class="row">
            <div class="col-lg-1 col-md-1 "></div>

            <div class="col-lg-1 col-md-2 ">
              <p class=" font-text">FIND THE </p>
              <p class="font-text2"> PERFECT MATCH</p>
              <p class="font-text3">FOR YOUR TEAM</p>
              <p class="fon-text4">SPARE 92% DEINER ZEIT BEI
                DEINEM IT-RECRUITING</p>
              <button class=" btn button2  ">MEHR ERFAHREN</button>
              <p class="font-text5">MEHR ERFAHREN</p>
            </div>

            <div class="col-lg-4 col-md-4 ">

              <img class="girl" src="assets/img/img2.png">

            </div>
          </div>

        </div>
        <br><br><br> <br><br><br>


        <div class="col-12 col-sm-12 col-md-12 col-lg-12 form-group">
          <div class="row">
            <div class="col-lg-2 col-md-2 "></div>
            <div class="col-lg-10 col-md-10 ">
              <p class="textf"><b>Why not do it yourself?</b></p>
              <p class="text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown </p>
            </div>

            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-1 col-md-1 ">
              <a href="" class=" ">
                <img class="buo" src="assets/img/rectangle.png">
              </a>
            </div>

            <div class="col-lg-3 col-md-3 ">
              <p class="text1"><b> 3 reasons why 90% of companies are not happy with their current HR agency</b></p>
              <p class="text2" v-show="texto1">{{ texto1.contenido}}</p>
            </div>

            <button type="button" class="btn ver" style="top: 50px;" data-toggle="modal" data-target="#modalCambiaTexto" @click="obtieneTextos()">
              <i class="fa fa-edit"> Modificar Textos</i>
            </button>


            <div class="col-lg-2 col-md-2"></div>
          </div>
        </div>



        <!-- inicio modal" -->
        <div class="modal fade bd-example-modal-lg" id="modalCambiaTexto" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header" style="background: linear-gradient(#FFFF62, #C234A6); color: #000 ">
                <h5 class="modal-title font-modal-title" id="exampleModalLabel">Modificar el texto </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                <section id="galeria" class="container">
                  <div class="row">
                    <!-- <div class="row" v-for="(texto,index) in textos"> -->

                    <label class="title-buscador">Selecciona un Texto</label>
                    <select @change="recuperaContenidoTexto()" class="form-control" v-model="id_texto">
                      <option value="0" selected>Seleccione</option>
                      <option v-for="(texto,index) in textos" :value="texto.id_texto"> {{texto.id_texto}} </option>
                    </select>
                  </div>
                </section>




                <div class="col-lg-4 col-md-4 col-sm-12 col-12 mt-2">
                  <div class="form-group text-left">
                    <label for="texto" class="">Texto Elegido: </label>
                    <div class="input-group">
                      <input type="text" class="" id="texto" aria-label="Text input with segmented dropdown button" placeholder="Texto Elegido" v-model="texto.input">

                    </div>
                  </div>
                </div>




              </div>
              <div class="modal-footer">

                <button type="button" class="btn buttonModal" @click="actualizaTexto()">Actualizar Texto </button>
                <button type="button" class="btn cerrar" data-dismiss="modal">Cerrar </button>
              </div>
            </div>
          </div>
        </div>
        <!--  Fin modal -->










        <div class="container ">
          <a href="" class=" ">
            <img class="buo2" src="assets/img/rec2.png" alt="">
          </a>
          <p class="t">FOCUS ON HIRING THE BEST</p>
          <p class="t1">Benefits of the bits&birds multi channel search</p>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
          <div class="row">
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-1 col-md-1 ">
              <a href="" class=" ">
                <img class="buo3" src="assets/img/rec3.png">
              </a>
            </div>
            <div class="col-lg-10 col-md-10 ">
              <p class="text8"><b> We can find anyone on social media</b></p>
              <p class="text9" v-show="texto2">{{ texto2.contenido}}</p>
            </div>
          </div>
        </div>


        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
          <div class="row">

            <div class="col-lg-1 col-md-1"></div>



            <div class="col-lg-1 col-md-1 ">
              <p class="text10"><b> We build fully enriched candidate profiles</b></p>
              <p class="text11" v-show="texto3">{{ texto3.contenido}}</p>
              <a class="text11   nav-font" style="top: 2380px;" href="">More about our candiate profiles>></a>
            </div>

            <div class="col-lg-10 col-md-10 ">
              <a href="" class=" ">
                <img class="buo4" src="assets/img/rec4.png">
              </a>
            </div>
          </div>
        </div>


        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
          <div class="row">

            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-1 col-md-1 ">
              <a href="" class=" ">
                <img class="buo5" src="assets/img/rec5.png">
              </a>
            </div>


            <div class="col-lg-10 col-md-10 ">
              <p class="text12"><b> We build our own software - CYBIRD</b></p>
              <p class="text13" v-show="texto4">{{ texto4.contenido}}</p>
            </div>
          </div>
        </div>




        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
          <div class="row">
            <div class="col-lg-1 col-md-1"></div>

            <div class="col-lg-1 col-md-1 ">
              <p class="text14"><b> We only hire highly trained human ressource professonals</b></p>
              <p class="text15" v-show="texto5">{{ texto5.contenido}}</p>
            </div>

            <div class="col-lg-10 col-md-10 ">
              <a href="" class=" ">
                <img class="buo6" src="assets/img/rec6.png">
              </a>
            </div>
          </div>
        </div>



      </div>

    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


    <!-- INICIO Footer  -->
    <footer class="footer fixed-bottom">
      <div class="container p-4">
        <div class="row">

          <div class="col-12 col-sm-12 col-md-4 col-lg-4">
            <div class="row">
              <br>
              <h5 class="fuenteFooter">Request more information</h5>
            </div>
          </div>

          <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
              <nav class="navbar navbar-light fuenteFooter2">
                <form class="form-inline">
                  <input class="form-control mr-sm-3" style="width: 630px;" type="search" placeholder="Type in your email address" aria-label="Search">
                  <button class="btn my-2 my-sm-0" style=" border-radius: 7px; background-color: #FFFF62; color: rgb(0, 0, 0); width: 275px; font-family: Barlow;font-style: normal; font-size: 18px; font-weight: 600;" type="submit">REQUEST MORE INFO</button>
                </form>
              </nav>
            </div>
          </div>

        </div>
      </div>


    </footer>
    <!-- FIN Footer  -->




  </div>


  <!-- LINK PARA RECONOCER VUE JS -->
  <script type="text/javascript" src="assets/js/vue.js"></script>
  <script type="text/javascript" src="assets/js/axios.min.js"></script>
  <script type="text/javascript" src="assets/js/sweetalert29.js"></script>



  <script type="text/javascript">
    const app = new Vue({
      el: '#index',
      data: {
        textos: [],
        texto1: [],
        texto2: [],
        texto3: [],
        texto4: [],
        texto5: [],
        textoRecuperado: [],
        id_texto: '0',

        texto: {
          input: '',
          mensaje: '',
          clase: '',
        },

      },
      methods: {


        actualizaTexto() {
          //id_texto = this.id_texto;
          // console.log("este EJSJDXND" + id_texto);
         // texto = this.texto.input;
          //console.log("TEXTO" + texto);
          axios.put('index.php/Texto_api/actualizaTexto', {
            texto: this.texto.input,
            id_texto: this.id_texto
          }).then(
            response => {
              textoNew = response.data;
              console.log(textoNew);

              Swal.fire({
                title: 'Texto Actualizado Con Éxito',
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK'
              }).then((result) => {
                if (result.value) {
                  window.location.href = 'index.php/Home/index';
                } else {
                  window.location.href = 'index.php/Home/index'; //Si el usuario da click fuera de la ventana
                }
              });
            }
          ).catch(error => {
            console.log("Error al Actualizar Texto");
             Swal.fire({
               position: 'center',
               icon: 'error',
               title: 'No se pudo actualizar el texto',
               showConfirmButton: false,
               timer: 1800
             })  
          });
        },

        recuperaContenidoTexto() {
          /* id_texto = this.id_texto;
           console.log("Este es el id_texto"+ id_texto); */
          axios.get('index.php/Texto_api/recuperaContenidoTexto', {
            params: {
              id_texto: this.id_texto
            }
          }).then(
            response => {
              this.textoRecuperado = response.data;
              console.log("ESTE ES TU TEXTO");
              console.log(this.textoRecuperado);
              this.texto.input = this.textoRecuperado.contenido;
            }
          ).catch(
            error => {
              console.error("No se pudo obtner el texto");
            }
          );
        },

        obtieneTextos() {
          axios.get('index.php/Texto_api/recuperaTextos').then(
            response => {
              this.textos = response.data;
              console.log("Estos son los textos");
              console.log(this.textos);
            }
          ).catch(
            error => {
              console.warn(error);
            });
        },


        recuperaTexto() {
          axios.get('index.php/Texto_api/recuperaTexto').then(
            response => {
              this.texto1 = response.data;
              console.log("Este es el texto");
              console.log(this.texto1);
            }
          ).catch(
            error => {
              console.warn(error);
            });
        },

        recuperaTexto2() {
          axios.get('index.php/Texto_api/recuperaTexto2').then(
            response => {
              this.texto2 = response.data;
              console.log("Este es el texto");
              console.log(this.texto2);
            }
          ).catch(
            error => {
              console.warn(error);
            });
        },

        recuperaTexto3() {
          axios.get('index.php/Texto_api/recuperaTexto3').then(
            response => {
              this.texto3 = response.data;
              console.log("Este es el texto");
              console.log(this.texto3);
            }
          ).catch(
            error => {
              console.warn(error);
            });
        },

        recuperaTexto4() {
          axios.get('index.php/Texto_api/recuperaTexto4').then(
            response => {
              this.texto4 = response.data;
              console.log("Este es el texto");
              console.log(this.texto4);
            }
          ).catch(
            error => {
              console.warn(error);
            });
        },

        recuperaTexto5() {
          axios.get('index.php/Texto_api/recuperaTexto5').then(
            response => {
              this.texto5 = response.data;
              console.log("Este es el texto");
              console.log(this.texto5);
            }
          ).catch(
            error => {
              console.warn(error);
            });
        },


      },
      created: function() {
        this.recuperaTexto();
        this.recuperaTexto2();
        this.recuperaTexto3();
        this.recuperaTexto4();
        this.recuperaTexto5();
        //setInterval(function(){ alert("Hello"); }, 5000);
      }
    });
  </script>


</body>

</html>